#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;94m'
BLACK='\033[0;30m'




HANDSHAKE_FILE=$1
##############################################
WORDLIST_BASENAME=wordlist.collection
ROOR_APP_DIR=/opt/WPA_Sandbox
ROOR_HANDSHAKE_DIR=$ROOR_APP_DIR/handshakes
WORDLIST_DIR=$ROOR_APP_DIR/$WORDLIST_BASENAME
AIRCRACK_OUTPUT_FOLDER=keys
WORDLIST=""
WORDLIST_FILES=""

##############################################

##############################################
#Depends
#+figlet
#+mlocate
#+aircrack-ng

function install_package()
{
	package=$1
	local res=
	dpkg -L $package > /dev/null 2>&1
	if [ $? -ne 0 ]
	then
	 echo -e "${YELLOW}Installing $package..."
	 sudo apt update > /dev/null 2>&1 && sudo apt install $package -y > /dev/null 2>&1
	 res=$?
	else
	 res=0
	fi
	return $res
}

echo -e "${BLUE}[ * ]${NOCOLOR} ${BLUE} Checking dependent software:"

install_package figlet
if [ $? -ne 0 ]
then
 echo -e "${RED}[ - ]${NOCOLOR} ${BLUE} figlet is not installed. Exit"
 exit
else
 echo -e "${GREEN}[ + ] figlet is installed"
fi


install_package mlocate
if [ $? -ne 0 ]
then
 echo -e "${RED}[ - ]${NOCOLOR} ${BLUE} mlocate is not installed. Exit"
 exit
else
 echo -e "${GREEN}[ + ] mlocate is installed"
fi

install_package aircrack-ng
if [ $? -ne 0 ]
then
 echo -e "${RED}[ - ]${NOCOLOR} ${BLUE} aircrack-ng is not installed. Exit"
 exit
else
 echo -e "${GREEN}[ + ] aircrack-ng is installed"
fi


figlet -f big "Wi-fire"


help () {
	main=`basename $0`
	echo -e "${RED}[ - ]${NOCOLOR} ${BLUE}Handshake missed. Usage: $main [PATH_TO_HANDSHAKE]"
}

status_help () {
	echo -e "${YELLOW}\n[ [s]tatus / [q]uit ]${BLACK}"
}

wordlist_update () {
	updatedb
	WORDLIST=`locate $WORDLIST_BASENAME`
	
	if [[ ! -z $WORDLIST ]] ; then
		all=0
		dirs=0
		for file in $WORDLIST ; do
			if [ -d $dirs ] ; then
				((dirs++))
			fi
			((all++))
		done
		
		WORDLIST_FILES=$(( all - dirs ))
		space=`du -sh $WORDLIST_DIR | cut -d$'\t' -f 1`
		echo -e "${GREEN}[ + ]${BLUE} Update wordlist database - - ${GREEN}[ Find $WORDLIST_FILES files / $space ]"
	fi
}

if [[ ! -z $HANDSHAKE_FILE ]] ; then
	shake=`basename $HANDSHAKE_FILE`
	echo -e "${GREEN}[ + ]${BLUE} Loading handshake ${GREEN}[ $shake ]"
else
	help
	exit 1
fi

if [[ -d $WORDLIST_DIR ]] ; then
	wordlist_update
else
	echo -e "${RED}[ - ]${BLUE} Wordlist path not found"
	exit 1
fi



if [[ ! -z $WORDLIST ]] ; then

	if [[ ! -d $ROOR_APP_DIR/$AIRCRACK_OUTPUT_FOLDER ]] ; then
		
		mkdir $ROOR_APP_DIR/$AIRCRACK_OUTPUT_FOLDER
	fi
	
	shake=`basename $HANDSHAKE_FILE`
	mv $HANDSHAKE_FILE $ROOR_HANDSHAKE_DIR/in.progress
	BSSID=`wpaclean $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/in.progress/$shake | grep -i net | cut -d' ' -f 2` 
	ESSID=`wpaclean $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/in.progress/$shake | grep -i net | cut -d' ' -f 3`
	found=$ROOR_APP_DIR/$AIRCRACK_OUTPUT_FOLDER/$ESSID
	STATUS_FILE=$ROOR_HANDSHAKE_DIR/in.progress/$shake.status
	
	echo -e "${GREEN}[ + ]${BLUE} Start cracking - - ${GREEN}[ `TZ="Europe/Kiev" date +%R\ %D` ]${BLACK}"
	
	if [ -f $found ] ; then
		key=`cat $found`
		echo -e "${GREEN}[ + ]${BLUE} Password found: ${YELLOW}[$ESSID] -> [$key] - - ${GREEN}[ `TZ="Europe/Kiev" date +%R\ %D` ]"
		
		if [ -f $ROOR_HANDSHAKE_DIR/in.progress/$shake ] ; then
			mv $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/done
		fi
		if [ -f $STATUS_FILE ] ; then
			rm $STATUS_FILE
		fi
		exit 0
	fi
	
	counter=0
	#((counter++))
	for file in $WORDLIST
	do
		if [[ -d $file ]] ; then
			continue
		else
			aircrack-ng -b $BSSID -l $found -w $file $ROOR_HANDSHAKE_DIR/in.progress/$shake > $STATUS_FILE &
			((counter++))
			#aircrack-ng -q -b $BSSID -l $found -w $file $ROOR_HANDSHAKE_DIR/in.progress/$shake >/dev/null
			while jobs | grep aircrack-ng | grep -v grep | grep -i running > /dev/null
			do
				read -n 1 -t 5 INPUT
				if [[ -z $INPUT ]] ; then
					continue
				elif [[ $INPUT == "s" ]] ; then
					aircrack_progress=`xxd $STATUS_FILE | grep % | tail -n 5 | awk -F ' ' '{ print $10}' | cut -d'%' -f1`
					aircrack_speed=`xxd $STATUS_FILE | grep "k/s" | tail -n 5 | awk -F '(' '{ print $2}' | cut -d' ' -f 1`
					speed=`echo $aircrack_speed | cut -d' ' -f 1`
					progress=""
					for x in $aircrack_progress ; do
						if [[ $x = *.* ]] ; then
							progress=$x
							break
						else
							continue
						fi
					done

					echo -e "\n${YELLOW}[ ? ] ${GREEN}( $counter/$WORDLIST_FILES ) ${BLUE} $file - - ${GREEN}[ Progress - $progress% / Speed -  $speed ]${BLACK}"
					unset INPUT
					continue
				elif [[ $INPUT == "q" ]] ; then
					echo -e "\n${RED}[ - ] Terminating..."
					kill -9 `jobs -l | grep aircrack-ng | grep -v grep | grep -i running | awk -F ' ' '{ print $2}'`
					unset INPUT
					mv $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/new
					rm $STATUS_FILE
					exit 0;
				else
					status_help
					unset INPUT
				fi
			done
			if [ -f $found ] ; then
				key=`cat $found`
				echo -e "${GREEN}[ + ]${BLUE} Password found: ${YELLOW}[$ESSID] -> [$key] - - ${GREEN}[ `TZ="Europe/Kiev" date +%R\ %D` ]"
				mv $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/done
				rm $STATUS_FILE
				exit 0
			fi
			echo -e "${RED}[ - ] ${GREEN}( $counter/$WORDLIST_FILES ) ${BLUE} $file - - ${GREEN}[ `TZ="Europe/Kiev" date +%R\ %D` ]${BLACK}"
		fi
		
	done
	
	echo -e "${RED}[ - ]${BLUE} Password not found: ${YELLOW}[$ESSID] -> [________] - - ${GREEN}[ `TZ="Europe/Kiev" date +%R\ %D` ]"
	mv $ROOR_HANDSHAKE_DIR/in.progress/$shake $ROOR_HANDSHAKE_DIR/done
	rm $STATUS_FILE
fi


